row_count = 6
column_count = 7
board = [['' for _ in range(column_count)] for _ in range(row_count)]


class Player:
    name: str
    chip: str

    def __init__(self, name: str, chip: str):
        self.name = name
        self.chip = chip


class Board:
    _empty_space = ' '
    _chip_one = 'X'
    _chip_two = 'O'

    _row_count = 6
    _column_count = 7
    _board: list[list]

    def __init__(self):
        self._board = [[self._empty_space for _ in range(self._column_count)]
                       for _ in range(self._row_count)]

    def print(self):
        for i in range(self._row_count):
            print(self._board[self._row_count - i - 1])

    def place_chip(self, player: Player, column: int) -> bool:
        if column > self._column_count or column < 1:
            print(
                f'Invalid column: {column}, must be between 1 and {self._column_count}')

            return False

        row_idx = 0
        column_idx = column - 1

        row_idx = self._check_column(row_idx, column_idx)

        if row_idx >= 0:
            self._board[row_idx][column_idx] = player.chip

            return True
        elif row_idx == -1:
            print('Column is full')
        else:
            print(f'Space is already occupied')

            return False

    def _check_column(self, row_idx: int, column_idx: int) -> int:
        if row_idx >= self._row_count:
            return -1
        elif self._board[row_idx][column_idx] == self._empty_space:
            return row_idx

        return self._check_column(row_idx + 1, column_idx)

    def contains_win(self) -> bool:
        return self._row_win()

    def _row_win(self):
        connect_count = {}
        prev_location = None

        for row in self._board:
            for location in row:
                if location != prev_location:
                    connect_count[location] = connect_count.get(location, 1) + 1
                else:
                    connect_count[location] = 0

                prev_location = location

        print(connect_count)

        return False



class Game:
    board: Board
    player_one: Player
    player_two: Player

    def __init__(self, board: Board, player_one: Player, player_two: Player):
        self.board = board
        self.player_one = player_one
        self.player_two = player_two

    def play_turn(self, player: Player) -> None:
        while True:
            user_input = input(f'Player {player.name} choose a column: ')
            column = int(user_input)

            if not self.board.place_chip(player, column):
                continue
            else:
                break

    def is_over(self) -> bool:
        return self.board.contains_win()


def open_row(column: int) -> int:
    i: int = row_count - 1

    while i >= 0:
        row = i

        location = board[row][column]

        if location == '':
            return row
        else:
            i -= 1

    return -1


def prompt_turn(player: str):
    chip: str

    if player == 'one':
        chip = 'X'
    elif player == 'two':
        chip = 'O'

    user_input = input(f'Player {player} choose a column\n')
    column = int(user_input)

    row = open_row(column)

    if row == -1:
        prompt_turn(player)

        return

    board[row][column] = chip


def check_win():
    last_chip = None

    for row in board:
        for chip in row:
            if last_chip is not None and last_chip == chip:
                print('match')
            else:
                last_chip = chip


if __name__ == '__main__':
    game = Game(Board(), Player('one', 'X'), Player('two', 'O'))

    while True:
        game.board.print()
        game.play_turn(game.player_one)

        if game.is_over():
            break

        game.board.print()
        game.play_turn(game.player_two)

        if game.is_over():
            break
